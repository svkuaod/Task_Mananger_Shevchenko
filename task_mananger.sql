
--
-- База данных: `task_mananger_shevchenko`
--
CREATE DATABASE IF NOT EXISTS `task_mananger_shevchenko`;
USE `task_mananger_shevchenko`;

-- --------------------------------------------------------

--
-- Структура таблицы `tasks`
--

DROP TABLE IF EXISTS `tasks`;
CREATE TABLE `tasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `text` TEXT NOT NULL,
  `status` enum('Not completed','Completed') NOT NULL DEFAULT 'Not completed',
  `task_image` varchar(255)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tasks`
--

INSERT INTO `tasks` (`user_name`, `user_email`, `text`, `status`,`task_image`) VALUES
  ('ivan', 'ivan@gmail.com', 'this is first task!!!', 'Completed', '/resources/img/1.jpg'),
  ('boris', 'boris@gmail.com', 'this is second task!!!', 'Not completed', '/resources/img/2.jpg'),
  ('petr', 'petr@gmail.com', 'this is third task', 'Not completed', '/resources/img/3.jpg'),
  ('fedor', 'fedor@gmail.com', 'this is fourth task', 'Not completed','/resources/img/default.jpg' );

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `login` varchar(255) NOT NULL  UNIQUE KEY,
  `password` varchar(255) NOT NULL,
  `role` enum('USER','ADMIN') NOT NULL DEFAULT 'USER'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`login`, `password`, `role`) VALUES
  ('admin','123', 'ADMIN'),
  ('user','123', 'USER');
