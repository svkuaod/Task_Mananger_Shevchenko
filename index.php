<?php
ini_set('display_errors', 1);
require_once './vendor/autoload.php';

define('ROOT', dirname(__FILE__));
session_start();
$router = new \App\Router();
$router->run();
