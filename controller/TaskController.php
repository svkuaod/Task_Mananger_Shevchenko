<?php

namespace Controller;

use App\ImageUpload;
use App\Pagination;
use DAO\TasksDAO;
use Model\Tasks;
use Validation\Validator;

class TaskController
{
    private $taskDao;
    private $task;
    private $imageUpload;

    public function __construct(){

        $this->taskDao = new TasksDAO();
        $this->task = new Tasks();
        $this->imageUpload = new ImageUpload();
    }


    /**
     * show page with tasks
     * @param int $pageNumber, const SORT_TYPE
     * @return boolean
     */
    public function actionIndex($pageNumber = 1,$selectedSortType = Tasks::SORT_BY_ORDER_UP){

        $sortTypeArray = Tasks::getSortTypeArray();
        $selectedSortType = $selectedSortType;
        $allTasksArr = $this->taskDao->getAll($this->task);
        $paginator = new Pagination($this->getSortedArrayByType($allTasksArr,$selectedSortType));
        $quantityOfPages = $paginator->getQuantityOfPages();
        $currentTasksArr = $paginator->getPaginateArrObject($pageNumber);
        require_once(ROOT . '/views/tasks/tasks.php');
        return true;
    }

    /**
     * show page with one selected tasks
     * @param int $id
     * @return boolean
     */
    public function actionView($id){

        if(Validator::checkId($id)) {
            $this->task->setId($id);
            $this->task = $this->taskDao->getById($this->task);
            require_once(ROOT . '/views/tasks/one_task.php');
            return true;
        }
        else false;
    }

    /**
     * show data for modal preview form
     * @return boolean
     */
    public  function actionPreview(){

        $this->task->setUserName($_POST['inputUserName']);
        $this->task->setUserEmail($_POST['inputUserEmail']);
        $this->task->setText($_POST['inputTaskText']);

        if ($fileUrl = $this->imageUpload->saveFile()) {
            $this->imageUpload->resizeFile($fileUrl);
        }
        else {
            $fileUrl = "resources/img/default.jpg";
        }

        $this->task->setTaskImage("/$fileUrl");
        echo $this->getPreviewViewForModalframe($this->task);
        return true;
    }


    /**
     * show create new task page
     * @return boolean
     */
    public function actionCreate()
    {
        if ($_POST) {
            $errors = false;
            $userName = $_POST['inputUserName'];
            $userEmail = $_POST['inputUserEmail'];
            $taskText = $_POST['inputTaskText'];

            if (!Validator::checkUserName($userName)) {
                $errors[] = 'Логин должен быть больше 3 символов но не более 30';
            }
            if (!Validator::checkUserEmail($userEmail)) {
                $errors[] = 'Неправильный email';
            }
            if (!Validator::checkTaskText($taskText)) {
                $errors[] = 'Слишком короткий текст задания';
            }

            if ($errors == false) {
                if ($fileUrl = $this->imageUpload->saveFile()) {
                    $this->imageUpload->resizeFile($fileUrl);
                } else $fileUrl = "resources/img/default.jpg";

                $this->task->setUserName($_POST['inputUserName']);
                $this->task->setUserEmail($_POST['inputUserEmail']);
                $this->task->setText($_POST['inputTaskText']);
                $this->task->setTaskImage("/$fileUrl");
                $this->taskDao->create($this->task);
                header('Location: \tasks');
            }
        }
        require_once(ROOT . '/views/tasks/create_task.php');
        return true;
    }

    /**
     * Returns sorted array of objects
     * @param array $arr, const SORT_TYPE
     * @return array
     */
    private function getSortedArrayByType($arr, $type){

        switch ($type){
            case (Tasks::SORT_BY_ORDER_UP):
                usort($arr, function($a, $b)
                {
                    return strcmp($a->getId(), $b->getId());
                });
                break;
            case (Tasks::SORT_BY_ORDER_DOWN):
                usort($arr, function($a, $b)
                {
                    return strcmp($b->getId(), $a->getId());
                });
                break;
            case (Tasks::SORT_BY_NAME_UP):
                usort($arr, function($a, $b)
                {
                    return strcmp($a->getUserName(), $b->getUserName());
                });
                break;
            case (Tasks::SORT_BY_NAME_DOWN):
                usort($arr, function($a, $b)
                {
                    return strcmp($b->getUserName(), $a->getUserName());
                });
                break;
            case (Tasks::SORT_BY_EMAIL_UP):
                usort($arr, function($a, $b)
                {
                    return strcmp($a->getUserEmail(), $b->getUserEmail());
                });
                break;
            case (Tasks::SORT_BY_EMAIL_DOWN):
                usort($arr, function($a, $b)
                {
                    return strcmp($b->getUserEmail(), $a->getUserEmail());
                });
                break;
            case (Tasks::SORT_BY_STATUS_UP):
                usort($arr, function($a, $b)
                {
                    return strcmp($a->getStatus(), $b->getStatus());
                });
                break;
            case (Tasks::SORT_BY_STATUS_DOWN):
                usort($arr, function($a, $b)
                {
                    return strcmp($b->getStatus(), $a->getStatus());
                });
                break;
        }
        return $arr;
    }

    /**
     * Returns view for modal frame preview
     * @param Task $task
     * @return string
     */
    private function getPreviewViewForModalframe($task){

        $output = '';
        $output .= '
            <div class="table-responsive">
                <table class="table table-bordered">';

        $output .= '
                <tr>
                     <img class="card-img-right flex-auto d-none d-md-block img-thumbnail" src="' . $task->getTaskImage() . '" alt="Card image cap">
                </tr>
                <tr>
                     <td width="30%"><label>User Name</label></td>
                     <td width="70%">' . $task->getUserName() . '</td>
                </tr>
                <tr>
                     <td width="30%"><label>User Email</label></td>
                     <td width="70%">' . $task->getUserEmail() . '</td>
                </tr>
                <tr>
                     <td width="30%"><label>Task description</label></td>
                     <td width="70%">' . $task->getText() . '</td>
                </tr>

                ';
        $output .= '
                    </table>
                </div>';
        return $output;
    }

}