<?php

namespace Controller;


use DAO\UsersDAO;
use Model\Users;
use Validation\Validator;

class UserController
{
    private $userDao;

    public function __construct()
    {
        $this->userDao = new UsersDAO();
    }

    /**
     * check entered value, save logined user to session and in case admin_role show admin panel
     *
     * @return boolean
     */
    public function actionLogIn(){

        $login = false;
        $password = false;

        if (isset($_POST['submit'])) {

            $login = $_POST['login'];
            $password = $_POST['password'];

            $errors = false;
            if (!Validator::checkUserName($login)) {
                $errors[] = 'Слишком короткий логин';
            }
            if (!Validator::checkPassword($password)) {
                $errors[] = 'Пароль не должен быть короче 6-ти символов';
            }

            $user = $this->userDao->getUserByLoginPassword($login, $password);

            if ($user == null) {
                $errors[] = 'Неправильные данные для входа на сайт';
            } else {
                $_SESSION['user'] = $user;

                if($user->getRole()===Users::ADMIN_ROLE){
                    header("Location: /admin");
                }
                else {
                    header("Location: /tasks");
                }
            }
        }
        require_once(ROOT . '/views/login/login.php');
        return true;
    }

    /**
     * delete current logined user from session and back to first page
     *
     * @return boolean
     */
    public function actionLogout(){

        unset($_SESSION['user']);
        header("Location: /tasks");
        return true;
    }

    /**
     * return true if user is logined
     *
     * @return boolean
     */
    public static function isLogined(){
        if ($_SESSION && isset($_SESSION['user'])) {
            return true;
        }
        return false;
    }
}