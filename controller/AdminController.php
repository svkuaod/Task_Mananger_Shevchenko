<?php

namespace Controller;

use DAO\TasksDAO;
use Model\Tasks;
use Model\Users;

class AdminController
{

    /**
     * Show the admin page if user has admin role.
     *
     * @return boolean
     */
    public function actionIndex()
    {
        if(self::checkAdmin()) {
            require_once(ROOT . '/views/admin/admin_page.php');
        }
        else header('Location: \login');

        return true;
    }

    /**
     * Show the edit tasks admin_page if user has admin role.
     *
     * @return boolean
     */
    public function actionTasks(){
        if(self::checkAdmin()) {
            $taskDAO = new TasksDAO();
            $entity = new Tasks();
            if (isset($_POST['buttonDelete'])) {
                $entity->setId($_POST['taskId']);
                $taskDAO->delete($entity);
            }
            else if(isset($_POST['buttonSave'])){
                $entity ->setId($_POST['taskId']);
                $entity ->setUserName($_POST['userName']);
                $entity ->setUserEmail($_POST['userEmail']);
                $entity ->setText($_POST['taskText']);
                $entity ->setTaskImage($_POST['taskImage']);
                if (isset($_POST['checkStatus'])){
                    $entity->setStatus(Tasks::STATUS_COMPLETED);
                }
                else $entity->setStatus(Tasks::STATUS_NOT_COMPLETED);
                $taskDAO->edit($entity);
            }

            $arrTasks = $taskDAO->getAll($entity);

            require_once(ROOT . '/views/admin/admin_tasks.php');
        }
        else header('Location: \login');
        return true;
    }

    /**
     * Show the edit users admin_page if user has admin role.
     *
     * @return boolean
     */
    public function actionUsers(){
        if(self::checkAdmin()) {
            require_once(ROOT . '/views/admin/admin_users.php');
            return true;
        }
    }

    /**
     * Check is logined user and has user admin role
     *
     * @return boolean
     */
    public static function checkAdmin(){

        if (UserController::isLogined()) {
            $user = $_SESSION['user'];
            if ($user->getRole() === Users::ADMIN_ROLE) {
                return true;
            }
        }
        else return false;
    }


}