<?php
/**
 * Created by PhpStorm.
 * User: slavik
 * Date: 22.01.2018
 * Time: 16:47
 */

namespace Model;


class Users
{
    const TABLE_NAME = 'users';
    const USER_ROLE = 'USER';
    const ADMIN_ROLE = 'ADMIN';

    private $id;
    private $login;
    private $password;
    private $role = self::USER_ROLE;

    /**
     * @return int
     */
    public function getId(){
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id){
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getLogin(){
        return $this->login;
    }

    /**
     * @param string $login
     */
    public function setLogin($login){
        $this->login = $login;
    }

    /**
     * @return string
     */
    public function getPassword(){
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword($password){
        $this->password = $password;
    }

    /**
     * @return const
     */
    public function getRole(){
        return $this->role;
    }

    /**
     * @param const $role
     */
    public function setRole($role){
        $this->role = $role;
    }


}