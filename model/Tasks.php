<?php

namespace Model;

class Tasks
{
    const TABLE_NAME = 'tasks';
    const STATUS_COMPLETED = "Completed";
    const STATUS_NOT_COMPLETED = "Not completed";
    const SORT_BY_ORDER_UP = 'order(A_Z)';
    const SORT_BY_ORDER_DOWN = 'order(Z_A)';
    const SORT_BY_NAME_UP = 'name(A_Z)';
    const SORT_BY_NAME_DOWN = 'name(Z_A)';
    const SORT_BY_EMAIL_UP = 'email(A_Z)';
    const SORT_BY_EMAIL_DOWN = 'email(Z_A)';
    const SORT_BY_STATUS_UP = 'status(completed_first)';
    const SORT_BY_STATUS_DOWN = 'status(not_completed_first)';

    /**
     * @return array with sort options
     */
    public static function getSortTypeArray(){
        $arr = array(self::SORT_BY_ORDER_UP,self::SORT_BY_ORDER_DOWN, self::SORT_BY_NAME_UP,self::SORT_BY_NAME_DOWN,
            self::SORT_BY_EMAIL_UP,self::SORT_BY_EMAIL_DOWN, self::SORT_BY_STATUS_UP,self::SORT_BY_STATUS_DOWN);
        return $arr;
    }

    private $id;
    private $user_name;
    private $user_email;
    private $text;
    private $status;
    private $task_image;

    /**
     * @return int
     */
    public function getId(){
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id){
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getUserName(){
        return $this->user_name;
    }

    /**
     * @param string $user_name
     */
    public function setUserName($user_name){
        $this->user_name = $user_name;
    }

    /**
     * @return string
     */
    public function getUserEmail(){
        return $this->user_email;
    }

    /**
     * @param string $user_email
     */
    public function setUserEmail($user_email){
        $this->user_email = $user_email;
    }

    /**
     * @return string
     */
    public function getText(){
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * @return enum
     */
    public function getStatus(){
        return $this->status;
    }

    /**
     * @param enum $status
     */
    public function setStatus($status){
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getTaskImage()
    {
        return $this->task_image;
    }

    /**
     * @param string $task_image
     */
    public function setTaskImage($task_image)
    {
        $this->task_image = $task_image;
    }

}