<?php

namespace Validation;

class Validator
{
    public static function checkUserName($userName) {
        if (strlen($userName) >= 3 && strlen($userName) <= 30) {
            return true;
        }
        return false;
    }

    public static function checkPassword($password) {
        if (strlen($password) >= 3 && strlen($password) <= 30) {
            return true;
        }
        return false;
    }

    public static function checkUserEmail($userEmail) {
        if (filter_var($userEmail, FILTER_VALIDATE_EMAIL)) {
            return true;
        }
        return false;
    }

    public static function checkTaskText($taskText) {

        if(filter_var($taskText,FILTER_SANITIZE_STRING) && strlen($taskText) >= 3)
            return true;
        return false;
    }

    public static function checkId($id){

        if(filter_var($id,FILTER_SANITIZE_NUMBER_INT)){
            return true;
        }
        return false;
    }

}