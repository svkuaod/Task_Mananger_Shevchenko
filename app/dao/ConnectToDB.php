<?php

namespace DAO;

class ConnectToDB
{
    const HOST = 'localhost';
    const USER = 'root';
    const PASS = '';
    const DB = 'task_mananger_shevchenko';
    protected static $connection;

    public function __construct(){
        self::$connection = self::connect();
    }

    /**
     * @param string $localhost
     * @param string $user
     * @param string $password
     * @param string $db
     * @return \mysqli
     */
    public static function connect($localhost = self::HOST, $user = self::USER, $password = self::PASS, $db = self::DB)
    {
        $link = mysqli_connect($localhost, $user, $password, $db);

        if (!$link) {
            echo 'err No: ' . mysqli_connect_errno() . PHP_EOL;
            echo 'error message: ' . mysqli_connect_error();
            die();
        }

        return $link;
    }

}