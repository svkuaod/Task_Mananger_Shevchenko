<?php

namespace DAO;

abstract class AbstractDAO extends ConnectToDB
{
    public abstract function create($entity);
    public abstract function edit($entity);

    public function delete($entity){
        $table = $entity::TABLE_NAME;
        $id = $entity->getId();
        $query = "DELETE FROM $table WHERE id = ?";
        $res = self::$connection->prepare($query);
        $res->bind_param('i', $id);
        $res->execute();
    }

    public function getById($entity){
        $className = get_class($entity);
        $table = $entity::TABLE_NAME;
        $id = $entity->getId();
        $query = "SELECT * FROM $table WHERE id = $id";
        $result = self::$connection->query($query);
        $obj = $result->fetch_object("$className");
        return $obj;
    }

    public function getAll($entity){
        $className = get_class($entity);
        $table = $entity::TABLE_NAME;
        $query = "SELECT * FROM $table";
        $objects = [];
        if ($result = self::$connection->query($query)) {
            while ($obj = $result->fetch_object("$className")) {
                $objects[] = $obj;
            }
        }
        return $objects;
    }
}