<?php
/**
 * Created by PhpStorm.
 * User: slavik
 * Date: 20.01.2018
 * Time: 0:44
 */

namespace DAO;


use Model\Tasks;

class TasksDAO extends AbstractDAO
{

    public function create($entity)
    {
        $userName = $entity->getUserName();
        $userEmail = $entity->getUserEmail();
        $text = $entity->getText();
        $status = Tasks::STATUS_NOT_COMPLETED;
        $taskImage = $entity->getTaskImage();
        $query = "INSERT INTO `tasks` ( user_name, user_email, text, status, task_image) VALUES  (?, ?, ?, ?, ?)";
        $res = self::$connection->prepare($query);
        $res->bind_param('sssss', $userName, $userEmail, $text, $status, $taskImage);
        $res->execute();
    }

    public function edit($entity)
    {
        $id = $entity->getId();
        $name = $entity->getUserName();
        $email = $entity->getUserEmail();
        $text = $entity->getText();
        $status = $entity->getStatus();
        $taskImage = $entity->getTaskImage();
        $query = 'UPDATE tasks SET user_name = ?, user_email = ?, text = ?, status = ?, task_image = ? WHERE id = ?';
        $res = self::$connection->prepare($query);
        $res->bind_param('sssssi', $name, $email, $text, $status, $taskImage, $id);
        $res->execute();
    }
}