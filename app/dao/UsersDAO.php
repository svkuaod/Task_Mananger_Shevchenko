<?php

namespace DAO;

class UsersDAO extends AbstractDAO{

    public function create($entity)
    {
        $login = $entity->getLogin();
        $password = $entity->getPassword();
        $role = $entity->getRole();
        $query = "INSERT INTO users ( login, password, role) VALUES  (?, ?, ?)";
        $res = self::$connection->prepare($query);
        $res->bind_param('sss', $login, $password, $role);
        $res->execute();
    }

    public function edit($entity)
    {
        $id = $entity->getId();
        $login = $entity->getLogin();
        $password = $entity->getPassword();
        $role = $entity->getRole();
        $query = 'UPDATE users SET login = ?, password = ?, role = ? WHERE id = ?';
        $res = self::$connection->prepare($query);
        $res->bind_param('sssi', $login, $password, $role, $id);
        $res->execute();
    }

    public function getUserByLoginPassword($login, $password)
    {
        $query = "SELECT * FROM `users` WHERE login = '$login' AND password = '$password'";
        $result = self::$connection->query($query);
        $obj = $result->fetch_object("Model\Users");
        return $obj;

    }

}