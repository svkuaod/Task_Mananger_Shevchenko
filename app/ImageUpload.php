<?php

namespace App;


class ImageUpload
{

    /**
     * If file was choosed (type jpg/png/gif) save it and rename if dublicate name
     *
     * @return string
     */
    public function saveFile()
    {
        if (isset($_FILES['fileToUpload']) && $_FILES['fileToUpload']['size'] > 0) {

            $target_dir = "resources/img/";
            $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
            $uploadOk = 1;

            $actual_name = pathinfo($target_file, PATHINFO_FILENAME);
            $original_name = $actual_name;
            $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
            $i = 1;
            while (file_exists($target_file)) {
                $actual_name = (string)$original_name . $i;
                $target_file = $target_dir . $actual_name . "." . $imageFileType;
                $i++;
            }

            if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "gif") {
                $uploadOk = 0;
            }

            if ($uploadOk == 0) {
                return null;
            } else {
                if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
                    return $target_file;
                } else {
                    return null;
                }
            }
        }
        else {
            return null;
        }
    }

    /**
     * Resize file if needed
     * @param string $srcImage
     */
    public function resizeFile($srcImage){

        $src = $this->imageCreateFromAny($srcImage);
        $w_src = imagesx($src);
        $h_src = imagesy($src);
        $image_type = exif_imagetype($srcImage);
        if ($w_src > 320) {
            $ratio = $w_src/320;
            $w_dest = round($w_src/$ratio);
            $h_dest = round($h_src/$ratio);
            $dest = imagecreatetruecolor($w_dest,$h_dest);

            switch ($image_type){
                case 1:
                    imagecopyresampled($dest, $src, 0, 0, 0, 0, $w_dest, $h_dest, $w_src, $h_src);
                    $background = imagecolorallocate($dest, 0, 0, 0);
                    imagecolortransparent($dest, $background);
                    imagegif($dest,$srcImage);
                    break;
                case 2:
                    imagecopyresized($dest, $src, 0, 0, 0, 0, $w_dest, $h_dest, $w_src, $h_src);
                    imagejpeg($dest,$srcImage,100);
                    break;
                case 3:
                    imagealphablending($dest, FALSE);
                    imagesavealpha($dest, TRUE);
                    imagecopyresampled($dest, $src, 0, 0, 0, 0, $w_dest, $h_dest, $w_src, $h_src);
                    imagepng($dest, $srcImage, 0);
                    break;
            }
            imagedestroy($dest);
            imagedestroy($src);
        }

        $src = $this->imageCreateFromAny($srcImage);
        $w_src = imagesx($src);
        $h_src = imagesy($src);
        if($h_src > 240){
            $ratio = $h_src/240;
            $w_dest = round($w_src/$ratio);
            $h_dest = round($h_src/$ratio);
            $dest = imagecreatetruecolor($w_dest,$h_dest);

            switch ($image_type){
                case 1:
                    imagecopyresampled($dest, $src, 0, 0, 0, 0, $w_dest, $h_dest, $w_src, $h_src);
                    $background = imagecolorallocate($dest, 0, 0, 0);
                    imagecolortransparent($dest, $background);
                    imagegif($dest,$srcImage);
                    break;
                case 2:
                    imagecopyresized($dest, $src, 0, 0, 0, 0, $w_dest, $h_dest, $w_src, $h_src);
                    imagejpeg($dest,$srcImage,100);
                    break;
                case 3:
                    imagealphablending($dest, FALSE);
                    imagesavealpha($dest, TRUE);
                    imagecopyresampled($dest, $src, 0, 0, 0, 0, $w_dest, $h_dest, $w_src, $h_src);
                    imagepng($dest, $srcImage, 0);
                    break;
            }
            imagedestroy($dest);
            imagedestroy($src);
        }
    }

    /**
     * Return image by file type
     * @param string $filepath
     * @return image
     */
    private function imageCreateFromAny($filepath) {
        $type = exif_imagetype($filepath);
        $allowedTypes = array(
            1,  // [] gif
            2,  // [] jpg
            3  // [] png
        );
        if (!in_array($type, $allowedTypes)) {
            return false;
        }
        switch ($type) {
            case 1 :
                $im = imageCreateFromGif($filepath);
                break;
            case 2 :
                $im = imageCreateFromJpeg($filepath);
                break;
            case 3 :
                $im = imageCreateFromPng($filepath);
                break;
        }
        return $im;
    }
}