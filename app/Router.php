<?php

namespace App;

class Router
{
    private $routes = [];

    public function __construct(){
        $routesPath = ROOT . '/resources/config/routes.php';
        $this->routes = include($routesPath);
    }

    public function run(){

        $uri = $this->getUri();

        foreach ($this->routes as $uriPattern => $path){

            if (preg_match("~$uriPattern~", $uri)){

                $internalRoute = preg_replace("~$uriPattern~", $path, $uri);
                $segments = explode ('/', $internalRoute);

                $controllerName = array_shift($segments) . 'Controller';
                $controllerName = ucfirst($controllerName);
                $actionName = 'action' . ucfirst(array_shift($segments));
                $parameters = $segments;

                $controllerFile = './controller/' . $controllerName . '.php';
                if(file_exists($controllerFile)){
                    $controllerClass = 'Controller\\' . $controllerName;
                }
                $controllerObject = new $controllerClass;

                $result = call_user_func_array(array($controllerObject,$actionName), $parameters);
                if($result != null){
                    break;
                }
            }

        }

    }

    /**
     * Returns request string
     */
    private function getUri(){
        if(!empty($_SERVER['REQUEST_URI'])){
            return trim($_SERVER['REQUEST_URI'],'/');
        }
    }

}