<?php

namespace App;


class Pagination
{
    private $arrObjects;
    private $objectsPerPage;
    private $quantityOfPages;
    private $quantityOfAllObjects;

    public function __construct($arrObjects,$objectsPerPage = 3)
    {
        $this->arrObjects = $arrObjects;
        $this->objectsPerPage = $objectsPerPage;
        $this->quantityOfPages = ceil(count($arrObjects)/$objectsPerPage);
        $this->quantityOfAllObjects = count($arrObjects);
    }

    /**
     * @return float
     */
    public function getQuantityOfPages(): float
    {
        return $this->quantityOfPages;
    }

    /**
     * Return array of objects for current page
     * @param int $page
     * @return array
     */
    public function getPaginateArrObject($page){

        $resultArrOfObjects = [];
        if(($page > 0)&&($page <= $this->quantityOfPages)){
            $indexOfCurrentObject = ($page - 1) * $this->objectsPerPage;
            for($i = 0; $i < $this->objectsPerPage; $i++){
                if($indexOfCurrentObject < $this->quantityOfAllObjects) {
                    $resultArrOfObjects[] = $this->arrObjects[$indexOfCurrentObject];
                    $indexOfCurrentObject++;
                }
                else break;
            }
            return $resultArrOfObjects;
        }
        return $resultArrOfObjects;
    }


}