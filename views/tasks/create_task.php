
<?php include ROOT . '/views/layouts/header.php'; ?>
<body>
<section class="jumbotron text-md-center">
    <div class="alert alert-success" role="alert">
        <h4 class="alert-heading">Create New Task Form!</h4>
        <hr>
        <?php if (isset($errors) && is_array($errors)): ?>
            <ul>
                <?php foreach ($errors as $error): ?>
                    <li> - <?php echo $error; ?></li>
                <?php endforeach; ?>
            </ul>
        <?php endif; ?>
    </div>
</section>
<div class="container">


        <form action="" method="post" id="formCreate" enctype="multipart/form-data">
        <div class="col align-self-center">
            <div class="form-group">
                <label for="inputUserName">User name</label>
                <input type="text" class="form-control" id="inputUserName" name="inputUserName" placeholder="enter user name">
            </div>
            <div class="form-group">
                <label for="inputUserEmail">Email address</label>
                <input type="email" class="form-control" id="inputUserEmail" name="inputUserEmail" placeholder="name@example.com">
            </div>
            <div class="form-group">
                <label for="inputTaskText">Enter task description</label>
                <textarea class="form-control" id="inputTaskText" name="inputTaskText" rows="3"></textarea>
            </div>

            <div class="input-group mb-3">
                <div class="custom-file">
                    <input type="file" class="custom-file-input" name="fileToUpload" id="image_upload" />
                    <label class="custom-file-label" for="image_upload" ></label>
                </div>
            </div>
<!--                <input type="file" name="fileToUpload" id="image_upload" />-->
<!--                <br />-->
                <button type="button" name="btnPreview" id="btnPreview" class="alert alert-success btnPreview" >Preview</button>
                <button type="submit" name="btnCreate" id="btnCreate" class="alert alert-success btnCreate" >Create</button>
                <a type="button" class="alert alert-info" href="\tasks">Back</a>
        </div>
        </form>


</div>
</body>

<div id="dataModal" class="modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title align-content-center">New Task Preview</h6>
            </div>
            <div class="modal-body" id="task_preview">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>



