<?php include ROOT . '/views/layouts/header.php'; ?>

    <main role="main">

        <section class="jumbotron text-md-left">
            <div class="row mb-2">
                <select name="sortSelect" onchange="location.href=this.value;" class="form-control">
                    <option value="">Sort by <?php echo $selectedSortType; ?></option>
                    <?php foreach ($sortTypeArray as $type): ?>
                        <option value="\tasks\<?php echo $pageNumber . '\\' . $type;?>"><?php echo $type;?></option>
                    <?php endforeach;?>
                </select>
            </div>
        </section>

        <div class="album py-5 bg-light">
            <div class="container">
                <?php foreach ($currentTasksArr as $task): ?>
                    <div class="row mb-3">
                        <div class="col-md-12">
                            <div class="card flex-md-row mb-4 box-shadow h-md-250">
                                <div class="card-body d-flex flex-column align-items-start">
                                    <strong class="d-inline-block mb-2 text-primary"><?php echo 'User :' . $task->getUserName()?></strong>
                                    <strong class="d-inline-block mb-2 text-primary"><?php echo 'Email adress: ' . $task->getUserEmail()?></strong>
                                    <strong class="d-inline-block mb-2 text-primary"><?php echo 'Status: ' . $task->getStatus()?></strong>
                                    <p class="card-text mb-auto"><?php echo $task->getText();?></p>
                                    <a href="\task\<?php echo $task->getId(); ?>">Continue reading</a>
                                </div>
                                <img class="card-img-right flex-auto d-none d-md-block img-thumbnail" src="<?php echo $task->getTaskImage();?>" alt="Card image cap">
                            </div>
                        </div>
                    </div>
                <?php endforeach;?>
                <nav aria-label="...">
                    <ul class="pagination justify-content-center">
                        <?php for($i = 1; $i <= $quantityOfPages; $i++): ?>
                            <li class="page-item"><a class="page-link" href="\tasks\<?php echo "$i\\$selectedSortType";?>"><?php echo $i;?></a></li>
                        <?php endfor; ?>
                    </ul>
                </nav>
            </div>
        </div>

    </main>

<?php include ROOT . '/views/layouts/footer.php'; ?>