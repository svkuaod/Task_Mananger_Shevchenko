<?php include ROOT . '/views/layouts/header.php'; ?>

<main role="main">
    <div class="album py-5 bg-light">
        <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card flex-md-row mb-4 box-shadow h-md-250">
                            <div class="card-body d-flex flex-column align-items-start">
                                <strong class="d-inline-block mb-2 text-primary"><?php echo 'User :' . $this->task->getUserName()?></strong>
                                <strong class="d-inline-block mb-2 text-primary"><?php echo 'Email adress: ' . $this->task->getUserEmail()?></strong>
                                <strong class="d-inline-block mb-2 text-primary"><?php echo 'Status: ' . $this->task->getStatus()?></strong>
                                <p class="card-text mb-auto"><?php echo $this->task->getText();?></p>
                                <a href="\tasks">Back</a>
                            </div>
                            <img class="card-img-right flex-auto d-none d-md-block img-thumbnail" src="<?php echo $this->task->getTaskImage();?>" alt="Card image cap">
                        </div>
                    </div>
                </div>
        </div>
    </div>
</main>

<?php include ROOT . '/views/layouts/footer.php'; ?>
