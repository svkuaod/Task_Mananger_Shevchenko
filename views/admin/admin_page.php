<?php include ROOT . '/views/layouts/header.php'; ?>

<body>
<section class="jumbotron text-md-center">
    <div class="alert alert-success" role="alert">
        <h4 class="alert-heading">Hello Admin!!</h4>
        <hr>
        <h6 class="alert-heading">You have such opportunities:</h6>
        <ul>
            <li><a href="\admin\tasks">Change tasks</a></li>
            <li><a href="\admin\users">Change users</a></li>
        </ul>
    </div>
</section>
</body>


<?php include ROOT . '/views/layouts/footer.php'; ?>
