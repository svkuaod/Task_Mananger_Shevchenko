<?php include ROOT . '/views/layouts/header.php'; ?>
<body>
<section class="jumbotron text-md-center">
    <div class="alert alert-success" role="alert">
        <h4 class="alert-heading">List of all tasks</h4>
        <a class="link" href="\admin">Back</a>
    </div>
    <button
</section>



<section class="jumbotron text-md-center">
<table class="table">
    <thead>
    <tr>
        <th scope="col">User name</th>
        <th scope="col">User email</th>
        <th scope="col">Text</th>
        <th scope="col">Task image</th>
        <th scope="col">Completed</th>
        <th scope="col">Action</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($arrTasks as $task): ?>
        <form method="post">
    <tr>
        <td scope="row">
            <input hidden type="text" class="form-control" name="taskId" value="<?php echo $task->getId(); ?>">
            <input type="text" class="form-control" name="userName" value="<?php echo $task->getUserName(); ?>">
        </td>
        <td>
            <input type="email" class="form-control" name="userEmail" value="<?php echo $task->getUserEmail(); ?>">
        </td>
        <td>
            <textarea type="text" class="form-control" name="taskText"><?php echo $task->getText(); ?></textarea>
        </td>
        <td>
            <input hidden type="text" class="form-control" name="taskImage" value="<?php echo $task->getTaskImage();?>">
            <img class="card-img-right flex-auto d-none d-md-block img-thumbnail"  src="<?php echo $task->getTaskImage();?>" alt="Card image cap">
        </td>
        <td>
            <input type="checkbox" name="checkStatus" <?php if($task->getStatus()==\Model\Tasks::STATUS_COMPLETED) { echo "checked";} ?>  >
            </td>
        <td>
            <button type="submit" name="buttonSave" class="btn-danger" value="buttonSave">Save</button>
            <button type="submit" name="buttonDelete" class="btn-danger" value="buttonDelete">Delete</button>

        </td>
    </tr>
        </form>
    <?php endforeach;?>
    </tbody>
</table>
</section>
</body>
<?php include ROOT . '/views/layouts/footer.php'; ?>