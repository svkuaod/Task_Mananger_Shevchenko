
<?php include ROOT . '/views/layouts/header.php'; ?>
<body>
<br>
<br>
<section>
    <div class="container">
        <div class="row">

            <div class="col-sm-4 col-sm-offset-4 padding-right">

                <?php if (isset($errors) && is_array($errors)): ?>
                    <ul>
                        <?php foreach ($errors as $error): ?>
                            <li> - <?php echo $error; ?></li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>

                <div class="signup-form"><!--sign up form-->
                    <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>
                    <form action="" method="post">
                        <input type="text" name="login" placeholder="enter login" value="<?php echo $login; ?>"/>
                        <input type="password" name="password" placeholder="enter password" value="<?php echo $password; ?>"/>
                        <input type="submit" name="submit" class="btn btn-default" value="Sign in" />
                    </form>
                </div>


                <br/>
                <br/>
            </div>
        </div>
    </div>
</section>
</body>
<?php include ROOT . '/views/layouts/footer.php'; ?>