<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Test task beejee">
    <meta name="author" content="Slavik Shevchenko">

    <title>Tasks mananger aplication</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="/resources/js/preview.js"></script>


</head>

<body>

<header>
    <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">

        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="\tasks">Show all tasks</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="\task\create">Add new task</a>
                </li>
                    <?php if (\Controller\AdminController::checkAdmin()):?>
                        <li class="nav-item">
                            <a class="nav-link" href="\admin">Admin panel</a>
                        </li>
                    <?php endif;?>
                <li class="nav-item">
                    <?php if(\Controller\UserController::isLogined()):?>
                        <a class="nav-link" href="\logout">Log out</a>
                    <?php else:?>
                        <a class="nav-link" href="\login">Log in</a>
                    <?php endif;?>
                </li>


            </ul>
        </div>
    </nav>

</header>