<?php

return array(
    'admin/tasks' => 'admin/tasks',
    'admin/users' => 'admin/users',
    'admin' => 'admin/index',
    'login' => 'user/logIn',
    'logout' => 'user/logOut',
    'task/([0-9]+)' => 'task/view/$1',
    'task/create/preview' => 'task/preview',
    'task/create' => 'task/create',
    'tasks/([0-9]+)' => 'task/index/$1',
    'tasks' => 'task/index',
    '' => 'task/index',
);
