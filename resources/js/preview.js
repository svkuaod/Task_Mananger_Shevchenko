$(document).ready(function(){
    $(document).on('click', '.btnPreview', function(){
        var data = new FormData();
        data.append('inputUserName',$('#inputUserName').val());
        data.append('inputUserEmail',$('#inputUserEmail').val());
        data.append('inputTaskText',$('#inputTaskText').val());
        data.append('fileToUpload', $('input[type=file]')[0].files[0]);
        $.ajax({
            url:"/task/create/preview",
            method:"POST",
            data:data,
            contentType: false,
            cache: false,
            processData: false,
            success:function(data){
                $('#task_preview').html(data);
                $('#dataModal').modal('show');
            }
        });
    });
});
