$(document).ready(function(){
    $(document).on('click', '.previewButton', function(){
        $.ajax({
            url:"/task/create/preview",
            method:"POST",
            data:{
                inputUserName: $('#inputUserName').val(),
                inputUserEmail: $('#inputUserEmail').val(),
                inputTaskText: $('#inputTaskText').val(),
            },
            success:function(data){
                $('#task_preview').html(data);
                $('#dataModal').modal('show');
            }
        });
    });
});